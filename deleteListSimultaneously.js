const apiKey = "fd3359bcaac13a0e64bdacbce2258945";
const token ="ATTAe1153f1b2e36202ee1703fe0c99af46c661d48bad45f9966d09c49410ff701304883E53A";
const getAllLists = require('./getLists');
const deleteList = require('./deleteList');

function deleteAllListsSimultaneously(boardID){
    return getAllLists(boardID)
        .then((listData) =>{
            let listId = listData.map((data) => data.id);
            return Promise.all(listId.map((id) => {
                deleteList(id)
            }))
        })
}


module.exports = deleteAllListsSimultaneously;
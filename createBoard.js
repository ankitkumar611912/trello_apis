const apiKey = "fd3359bcaac13a0e64bdacbce2258945";
const token = "ATTAe1153f1b2e36202ee1703fe0c99af46c661d48bad45f9966d09c49410ff701304883E53A";
const permission = "public";

function createBoard(boardName) {
  const url = `https://api.trello.com/1/boards/?name=${boardName}&prefs_permissionLevel=${permission}&defaultLists=false&key=${apiKey}&token=${token}`;
  return fetch(url, {
        method: "POST",
    })
    .then((response) => {
      return response.json();
    })
    .catch((err) => new Error("Not valid url"));
}

module.exports = createBoard;

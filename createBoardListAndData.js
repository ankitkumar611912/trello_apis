const apiKey = "fd3359bcaac13a0e64bdacbce2258945";
const token = "ATTAe1153f1b2e36202ee1703fe0c99af46c661d48bad45f9966d09c49410ff701304883E53A";
const createBoard = require('./createBoard');
const createCard = require('./createCard');
const createList = require('./createList');

function createBoardListAndData(boardName){
    return createBoard(boardName)
        .then((boardData) => {
            console.log(boardData);
            let listArray = [];
            for(let index = 0 ; index < 3; index++){
                listArray.push(createList(boardData.id,index+1));
            }
            return Promise.all(listArray);
        })
        .then((listData) =>{
            console.log(listData);
            let listId = listData.map(data => data.id);
            return Promise.all(listId.map((listdata) => createCard(listdata,"hello")));
        })
        
}


module.exports = createBoardListAndData
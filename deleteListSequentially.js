const apiKey = "fd3359bcaac13a0e64bdacbce2258945";
const token ="ATTAe1153f1b2e36202ee1703fe0c99af46c661d48bad45f9966d09c49410ff701304883E53A";
const getAllLists = require('./getLists');
const deleteList = require('./deleteList');

// function deleteListData(listId, index) {
//     return new Promise((res, rej) => {
//         if(index >= listId.length) {
//             res();
//         }else {
//             deleteList(listId[index])
//                 .then(() => deleteListData(listId, index + 1))
//                 .then(() => res());
//         }
//     });
// }


function deleteListSequentialy(boardID){
    return getAllLists(boardID)
        .then((listData) =>{
            let deletePromise = Promise.resolve();
            listData.forEach((data) =>{
                deletePromise = deletePromise.then(()=> deleteList(data.id));
            })
            return deletePromise;
        })
}

module.exports = deleteListSequentialy;
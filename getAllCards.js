const apiKey = "fd3359bcaac13a0e64bdacbce2258945";
const token = "ATTAe1153f1b2e36202ee1703fe0c99af46c661d48bad45f9966d09c49410ff701304883E53A";
const getCardData = require('./getCardData');
const getAllLists = require('./getLists');

function getAllCards(boardId){
    return getAllLists(boardId)
    .then((listData) =>{
        let listId = listData.map((list) => {
            return list.id;
        })
        return Promise.all(listId.map((listdata) => getCardData(listdata)));
    })
    
}


module.exports = getAllCards;
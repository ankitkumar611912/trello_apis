const apiKey = "fd3359bcaac13a0e64bdacbce2258945";
const token = "ATTAe1153f1b2e36202ee1703fe0c99af46c661d48bad45f9966d09c49410ff701304883E53A";
const UncheckItemsAll = require("./unCheckItem");
const getAllCheckList = require("./getAllChecklist");

function getCheckItemsData(checkListId) {
  return fetch(
    `https://api.trello.com/1/checklists/${checkListId[0]}/checkItems?key=${apiKey}&token=${token}`,
    {
      method: "GET",
    }
  )
    .then((response) => {
      return response.json();
    })
    .then((checkitemdata) => {
        // const delayedCheck = (data, index) => {
        //     return new Promise((resolve) => {
        //       if (index === 0) {
        //         resolve(UncheckItemsAll(data.id, checkListId[1]));
        //       } else {
        //         setTimeout(() => {
        //           resolve(UncheckItemsAll(data.id, checkListId[1]));
        //         }, 1000 * index); 
        //       }
        //     });
        // };
        // return Promise.all(checkitemdata.map((data, index) => delayedCheck(data, index)));
        // let uncheckPromise = Promise.resolve();
        checkitemdata.forEach((checkItemsData,index) =>{
          setTimeout(() =>{
            UncheckItemsAll(checkItemsData.id,checkListId[1]);
            console.log(`Unchecked ${checkItemsData.id}`)
          },index*1000)
        });
        return Promise.resolve();
    })
}

function updateAllCheckitmesSequentially(boardID) {

    return  getAllCheckList(boardID)
        .then((checkList) => {

            let checkListId = checkList.reduce((prevData, currData) => {
                let newCurrData = [];
                newCurrData.push(currData.id);
                newCurrData.push(currData.idCard);
                prevData.push(newCurrData);
                return prevData;
            }, []);
            return checkListId;
        })
        .then((checkListId) => {
            return Promise.all(checkListId.map((id) => getCheckItemsData(id)));
        })
        .then(() => "All checkitems in checklist is incompeleted sequentially")
}


module.exports = updateAllCheckitmesSequentially

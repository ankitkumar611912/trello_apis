const apiKey = "fd3359bcaac13a0e64bdacbce2258945";
const token ="ATTAe1153f1b2e36202ee1703fe0c99af46c661d48bad45f9966d09c49410ff701304883E53A";

function getAllLists(boardID) {
  const url = `https://api.trello.com/1/boards/${boardID}/lists?key=${apiKey}&token=${token}`;
  return fetch(url, {
    method: "GET",
    headers: {
      Accept: "application/json",
    },
  })
    .then((response) => {
      return response.json();
    })
    .catch((err) => (err));
}


module.exports = getAllLists;